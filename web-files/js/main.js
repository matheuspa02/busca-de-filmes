// FUNÇÃO QUE REALIZA A PESQUISA NA API

const API_KEY = '48161b8a7e7623a36ee3da470200a8c6';
const IMAGE_URL = 'https://image.tmdb.org/t/p/w500';

const url = 'https://api.themoviedb.org/3/search/movie?api_key=48161b8a7e7623a36ee3da470200a8c6';


const buttonElement = document.querySelector('#search');
const inputElement = document.querySelector('#inputValue');
const userMovieSearch = document.querySelector('#movies-user-search-results');
const playingMovies = document.querySelector('#now-playing-movies');

// funçao que gera a Url

function generateUrl(path) {
    const url = `https://api.themoviedb.org/3${path}?api_key=48161b8a7e7623a36ee3da470200a8c6`;
    return url;
}

// essa parte em específico faz a busca na api
function movieApiSearch(data) {
    userMovieSearch.innerHTML = '';
    const movies = data.results;
    // const movieBlock = createUserSearchMovieContainer(movies);
    // userMovieSearch.appendChild(movieBlock);
    // console.log('Data: ', movies);
    for(i=0; i<4; i++){
        // console.log('Movies: ', movies[i]);
        
        userMovieSearch.appendChild(createUserSearchMovieContainer(movies[i]));
        // console.log('Teste');
        // createUserSearchMovieContainer(movies[i]);
        // console.log('Valor do i:', i);
    }
}

// FAZ A PESQUISA DOS FILMES EM CARTAZ

function searchNowPlayingMovies(data) {
    const movies = data.results;
    // const movieBlock = createUserSearchMovieContainer(movies);
    // userMovieSearch.appendChild(movieBlock);
    // console.log('Data: ', movies);
    for(i=0; i<4; i++){
        // console.log('Movies: ', movies[i]);
        
        playingMovies.appendChild(createNowPlayingMovieContainer(movies[i], this.title));
        console.log(movies[i]);
        // createUserSearchMovieContainer(movies[i]);
        // console.log('Valor do i:', i);
    }
}

// FAZ A REQUISIÇÃO NA API

function requestMovies(url, onComplete) {
    fetch(url)
    .then((res) => res.json())
    .then(onComplete)
        .catch((error) => {
            // console.log('Erro: ', error);
        });
}

// FUNÇÃO QUE FAZ A URL QUE PROCURA O FILME

function searchMovie(value) {
    const path = '/search/movie';
    const url = generateUrl(path) + '&query=' + value;
    requestMovies(url, movieApiSearch);
}

// FUNÇÃO QUE FAZ A URL QUE PROCURA OS FILMES EM CARTAZ

function nowPlayingMovie() {
    const path = '/movie/now_playing';
    const url = generateUrl(path);

    const render = searchNowPlayingMovies.bind({title: 'Em cartaz nos cinemas'});

    requestMovies(url, render);
}

// FUNÇÃO QUE FAZ A URL QUE PROCURA OS FILMES TOP RATE

function topRateMovies() {
    const path = '/movie/top_rated';
    const url = generateUrl(path);
    
    requestMovies(url, movieApiSearch);
}

// FUNÇÃO QUE PEGA O VALOR DO BOTÃO PARA FAZER A PESQUISA
buttonElement.onclick = function(event) {
    event.preventDefault();
    const value = inputElement.value;
    searchMovie(value);

    // console.log('Value: ', value);
    inputElement.value = '';
}
// FIM DA FUNÇÃO QUE REALIZA A PESQUISA NA API


// VAI CRIAR O CONTAINER COM O FILME PESQUISADO PELO USUÁRIO

function createUserSearchMovieContainer(movie) {

    const movieElement = document.createElement('div');
    movieElement.setAttribute('class', 'card');

    const movieTemplate = `
                <div class="card-aside">
                    <div class="card-aside-score">
                        <span>${movie.vote_average}</span>Média de Votos
                    </div>
                    <p><img class="card-aside-img" src=${IMAGE_URL + movie.backdrop_path} data-movie-id=${movie.id}/></p>
                </div>   
                <div class="card-main">
                    <div class="card-front"> 
                        <img class="card-poster" src=${IMAGE_URL + movie.poster_path} data-movie-id=${movie.id}/>
                    </div>
                    <div class="card-back"> <img src=""> 
                        <h3 class="card-back-title">${movie.title}</h3>
                    <div class="card-back-description">
                        ${movie.overview}
                    </div>
                </div>
    `;

    movieElement.innerHTML = movieTemplate;
    return movieElement;
}

// criar o container dos filmes em cartaz

function createNowPlayingMovieContainer(movie, title) {

    const movieElement = document.createElement('div');
    movieElement.setAttribute('class', 'screen');

    const movieTemplate = `
    <img class="now-playing-banner" src=${IMAGE_URL + movie.poster_path} data-movie-id=${movie.id}/>
    <div class="content-now-playing">
        <h2>${movie.title}</h2>
        <p>${movie.overview}</p>
    </div>
    `;
    // <p>${movie.overview}</p>

    movieElement.innerHTML = movieTemplate;
    return movieElement;
}








nowPlayingMovie();
// topRateMovies();